package ru.ragetf.vk.data.network

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import ru.ragetf.vk.data.network.vk.VkModule
import javax.inject.Singleton

@Module(includes = [VkModule::class])
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()

}