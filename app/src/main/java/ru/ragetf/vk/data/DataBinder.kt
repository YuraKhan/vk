package ru.ragetf.vk.data

import dagger.Binds
import dagger.Module
import ru.ragetf.vk.data.dao.CredentialsDao
import ru.ragetf.vk.data.dao.FriendsDao
import ru.ragetf.vk.data.dao.impls.CredentialsDaoImpl
import ru.ragetf.vk.data.dao.impls.FriendsDaoImpl

@Module
abstract class DataBinder {

    @Binds
    abstract fun bindFriendsDao(dao: FriendsDaoImpl): FriendsDao
    @Binds
    abstract fun bindCredentialsDao(dao: CredentialsDaoImpl): CredentialsDao

}