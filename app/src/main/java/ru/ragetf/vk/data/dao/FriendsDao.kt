package ru.ragetf.vk.data.dao

import io.reactivex.Observable
import ru.ragetf.vk.data.dto.FriendDto

interface FriendsDao {

    fun getMyFriends(): Observable<FriendDto>
    fun getFriends(userId: Int?) : Observable<FriendDto>

}