package ru.ragetf.vk.data

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DataQualifier(val name: String = "")