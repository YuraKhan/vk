package ru.ragetf.vk.data

import kotlin.NoSuchElementException

fun <T> T?.throwIfNull(): T {
    return this ?: throw NoSuchElementException()
}
