package ru.ragetf.vk.data.mapper

import io.reactivex.functions.Function
import ru.ragetf.vk.data.dto.FriendDto
import ru.ragetf.vk.data.network.vk.pojo.payload.friends.Friend
import ru.ragetf.vk.data.throwIfNull

class FriendDtoMapper : Function<Friend, FriendDto> {
    override fun apply(t: Friend): FriendDto {
        return FriendDto(
            t.id.throwIfNull(),
            t.lastName.throwIfNull(),
            t.firstName.throwIfNull(),
            t.photo50.throwIfNull()
        )
    }

}