package ru.ragetf.vk.data.dto

data class FriendDto(
    val id: Int,
    val lastName: String,
    val firstName: String,
    val photoUrl: String
)