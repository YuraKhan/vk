package ru.ragetf.vk.data.network.vk.pojo.payload

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Country{
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
}