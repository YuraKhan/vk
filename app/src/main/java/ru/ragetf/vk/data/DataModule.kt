package ru.ragetf.vk.data

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import ru.ragetf.vk.data.network.NetworkModule
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, DataBinder::class])
class DataModule {

    @Provides
    @Singleton
    @DataQualifier("credentialsPreferences")
    fun provideCredentialsPreferences(context: Context): SharedPreferences = context.getSharedPreferences("credentials", Context.MODE_PRIVATE)

}