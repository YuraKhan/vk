package ru.ragetf.vk.data.dao.impls

import io.reactivex.Observable
import ru.ragetf.vk.data.dao.CredentialsDao
import ru.ragetf.vk.data.dao.FriendsDao
import ru.ragetf.vk.data.dto.FriendDto
import ru.ragetf.vk.data.mapper.FriendDtoMapper
import ru.ragetf.vk.data.network.vk.Fields
import ru.ragetf.vk.data.network.vk.FriendOrder
import ru.ragetf.vk.data.network.vk.api.FriendsApi
import ru.ragetf.vk.data.throwIfNull
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FriendsDaoImpl @Inject constructor() : FriendsDao {

    @Inject
    lateinit var friendsApi: FriendsApi
    @Inject
    lateinit var credentialsDao: CredentialsDao

    override fun getFriends(userId: Int?): Observable<FriendDto> {
        return credentialsDao.getToken().flatMap {
            friendsApi.getFriends(
                it.token,
                userId,
                FriendOrder.HINTS,
                null,
                null,
                null,
                listOf(Fields.PHOTO_50)
            )
        }.flatMapObservable {
            Observable.fromIterable(it.items)
        }.map(FriendDtoMapper())
    }

    override fun getMyFriends(): Observable<FriendDto> {
        return getFriends(null)
    }


}