package ru.ragetf.vk.di

import dagger.Module
import dagger.Provides
import ru.ragetf.vk.presentation.ui.root.MainQualififer
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class RouterModule {

    @Provides
    @MainQualififer
    @Singleton
    fun provideMainRouter(): Router = Router()

}