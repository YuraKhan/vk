package ru.ragetf.vk.presentation.ui.root.friends

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.ragetf.vk.data.dao.FriendsDao
import ru.ragetf.vk.presentation.base.MoxyPresenter
import ru.ragetf.vk.presentation.mapper.FriendModelMapper
import javax.inject.Inject

@InjectViewState
class FriendPresenter @Inject constructor() : MoxyPresenter<FriendView>() {

    @field:Inject
    @field:JvmField
    @field:FriendQualifier("userId")
    var userId: Int = 0

    @field:Inject
    lateinit var friendsDao: FriendsDao

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        friendsDao.getFriends(if (userId > 0) userId else null)
            .map(FriendModelMapper())
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.showFriendList(it)
            }, {
                it.printStackTrace()
            }).toCompositeDisposable()
    }

}