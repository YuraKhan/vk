package ru.ragetf.vk.presentation.ui.root

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope