package ru.ragetf.vk.presentation.ui.root

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.ragetf.vk.presentation.ui.root.friends.FriendFragment
import ru.ragetf.vk.presentation.ui.root.friends.FriendModule
import ru.ragetf.vk.presentation.ui.root.friends.FriendScope
import ru.ragetf.vk.presentation.ui.root.login.LoginFragment

@Module
abstract class MainFragmentBuilder {

    @ContributesAndroidInjector(modules = [FriendModule::class])
    @FriendScope
    abstract fun buildFriendFragment(): FriendFragment

    @ContributesAndroidInjector
    abstract fun buildLoginFragment(): LoginFragment

}