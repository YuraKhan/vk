package ru.ragetf.vk.presentation.ui.root.friends

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FriendScope(val name: String = "")