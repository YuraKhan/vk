package ru.ragetf.vk.presentation.base

import android.content.Context
import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


abstract class DiMoxyDialogFragment : MoxyDialogFragment(), HasSupportFragmentInjector {

    @field:Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }

}