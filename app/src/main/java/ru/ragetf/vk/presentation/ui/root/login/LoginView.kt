package ru.ragetf.vk.presentation.ui.root.login

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.ragetf.vk.presentation.base.BaseView

interface LoginView: BaseView{

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLogin()

}