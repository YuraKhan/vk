package ru.ragetf.vk.presentation.ui

import ru.ragetf.vk.presentation.ui.root.friends.FriendScreen
import ru.ragetf.vk.presentation.ui.root.login.LoginScreen
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenFactory {

    fun getFriendScreen(userId: Int? = null): SupportAppScreen {
        return FriendScreen(userId)
    }

    fun getLoginScreen(): SupportAppScreen {
        return LoginScreen()
    }

}