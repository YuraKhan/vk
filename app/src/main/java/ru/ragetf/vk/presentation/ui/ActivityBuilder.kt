package ru.ragetf.vk.presentation.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.ragetf.vk.presentation.ui.root.MainActivity
import ru.ragetf.vk.presentation.ui.root.MainModule
import ru.ragetf.vk.presentation.ui.root.MainScope

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainModule::class])
    @MainScope
    abstract fun buildMainActivity(): MainActivity

}