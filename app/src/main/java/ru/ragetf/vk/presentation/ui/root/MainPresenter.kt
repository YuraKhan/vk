package ru.ragetf.vk.presentation.ui.root

import com.arellomobile.mvp.InjectViewState
import com.vk.sdk.VKAccessToken
import io.reactivex.functions.Consumer
import ru.ragetf.vk.data.dao.CredentialsDao
import ru.ragetf.vk.presentation.base.MoxyPresenter
import ru.ragetf.vk.presentation.ui.ScreenFactory
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor() : MoxyPresenter<MainView>() {

    @Inject
    @field:MainQualififer
    lateinit var router: Router

    @Inject
    lateinit var credentialsDao: CredentialsDao

    fun onCreate(isRestore: Boolean) {
        if (!isRestore)
            credentialsDao.isTokenExists().subscribe(Consumer {
                if (it)
                    router.newRootScreen(ScreenFactory.getFriendScreen())
                else
                    router.newRootScreen(ScreenFactory.getLoginScreen())
            }).toCompositeDisposable()
    }

}