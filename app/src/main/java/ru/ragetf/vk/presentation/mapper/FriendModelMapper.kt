package ru.ragetf.vk.presentation.mapper

import io.reactivex.functions.Function
import ru.ragetf.vk.data.dto.FriendDto
import ru.ragetf.vk.presentation.model.FriendModel

class FriendModelMapper : Function<FriendDto, FriendModel> {
    override fun apply(t: FriendDto): FriendModel {
        return FriendModel(t.id, "${t.lastName} ${t.firstName}", t.photoUrl)
    }
}