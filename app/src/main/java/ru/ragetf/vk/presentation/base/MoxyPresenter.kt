package ru.ragetf.vk.presentation.base

import android.content.Context
import android.os.Bundle
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

abstract class MoxyPresenter<T : MvpView> : MvpPresenter<T>() {

    protected var destroyDisposable = CompositeDisposable()
    protected lateinit var context: Context

    override fun onDestroy() {
        super.onDestroy()
        if (!destroyDisposable.isDisposed)
            destroyDisposable.dispose()
    }

    protected fun Disposable.toCompositeDisposable(): Boolean {
        return destroyDisposable.add(this)
    }

}