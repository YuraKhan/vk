package ru.ragetf.vk.presentation.ui.root

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MainQualififer(val name: String = "")