package ru.ragetf.vk.presentation.ui.root.friends

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.ragetf.vk.presentation.base.BaseView
import ru.ragetf.vk.presentation.model.FriendModel

interface FriendView : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showFriendList(list: List<FriendModel>)

}