package ru.ragetf.vk.presentation.ui.root.login

import com.arellomobile.mvp.InjectViewState
import ru.ragetf.vk.data.dao.CredentialsDao
import ru.ragetf.vk.presentation.base.MoxyPresenter
import ru.ragetf.vk.presentation.ui.ScreenFactory
import ru.ragetf.vk.presentation.ui.root.MainQualififer
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LoginPresenter @Inject constructor() : MoxyPresenter<LoginView>() {

    @Inject
    lateinit var credentialsDao: CredentialsDao
    @Inject
    @field:MainQualififer
    lateinit var router: Router

    fun onSignIn() {
        viewState.showLogin()
    }

    fun onTokenReceive(userId: String, token: String) {
        credentialsDao.saveToken(CredentialsDao.VkToken(userId, token))
            .subscribe {
                router.newRootScreen(ScreenFactory.getFriendScreen())
            }.toCompositeDisposable()
    }

    fun onError(code: Int){

    }

}